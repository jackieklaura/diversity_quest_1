# Diversity Quest \#1

Ein Konzept für einen eintägigen Workshop zur Strategiefindung, um im Coding-Kontext die Diversity zu erhöhen. Das Konzept ist zugeschnitten auf das Jugend Hackt Netzwerk und deren Workshop, mit dem Fokus darauf, wie mehr Mädchen / junge Frauen für Coding und die Workshops/Hackathons von Jugend Hackt begeistert werden können. In der Form ist es an andere Formen von Diversity und andere organisationale Kontexte anpassbar. Vor allem bietet sich der ersten Teil auch für Vernetzungsevents an.

# Hintergrund

[Jugend hackt](https://jugendhackt.org) ist ein Programm der [Open Knowledge Foundation](https://okfn.de) zur Förderung des Programmiernachwuchses im deutschsprachigen Raum. Wie in fast allen Coding-bezogenen Kontexten im deutschsprachigen Raum sind die Teilnehmer\*innen von Jugend hackt Events zum Großteil weiß und männlich. Jugend hackt setzt bereits einige Maßnahmen um, die auf die Erhöhung der Diversity abzielen. Um weitere Strategien zu entwickeln, wurde dazu für die Organisator\*innen im Jugend hackt Netzwerk am 11. Januar 2019 ein Workshoptag in Berlin organisiert.

Das Konzept für diesen Workshoptag wird hier vorgestellt. Alle Inhalte stehen unter einer [CC-BY-SA 4.0](http://creativecommons.org/licenses/by/4.0/) Lizenz. Über Beiträge, Forks und Vernetzung freue ich mich.

# Workshopdesign

![](01_welcome.png)

TODO: text

## Quest Schedule

![](02_schedule.png)

TODO: text

## Feine Zeitplanung

TODO: timetable

## Main Quest

TODO: text

![](03_quest.png)

![](04_best_practice.png)

![](04_worst_case.png)

# Hinweise zur Adaption

TODO: text

# Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.
